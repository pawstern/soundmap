import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import math
from playsound import playsound
import hrtf

class Main(QWidget):

    def __init__(self):
        super().__init__()
        self.initUI()
        self.setMouseTracking(True)
    
    def initUI(self):
        input_file = 'drum.wav'
        self.output_directory = 'output'
        hrtf_model = 'subject1_HRTF.mat'
        hrtf.createTracks(input_file, self.output_directory, hrtf_model)
        self.resize(300, 300)
        self.setWindowTitle("Sound map")
        self.show()

    def mousePressEvent(self, event):
        x = event.x()-150
        y = -event.y()+150
        if (y == 0):
            y = 1
        if (x == 0):
            x =1
        soundPosition = math.degrees(math.atan(x/y))
        if ( (y < 0) and (x < 0)):
            soundPosition = soundPosition - 180
        elif ((y < 0) and (x > 0)): 
            soundPosition = soundPosition + 180
        else:
            soundPosition = soundPosition 

        print(soundPosition)
        if (soundPosition < 0):
            trackNumber = round(((abs(soundPosition))/180) * 35)

        else:
            trackNumber = 72 - math.ceil(((soundPosition/180) * 35))

        if (math.sqrt(x*x + y*y) < 30):
            playsound(self.output_directory+'/track{}_near.wav'.format(trackNumber))
            print("Play track{}_near.wav".format(trackNumber))

        else:
            playsound(self.output_directory+'/track{}_far.wav'.format(trackNumber))
            print("Play track{}_near.wav".format(trackNumber))

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.begin(self)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setBrush(Qt.black)
        painter.setPen(QPen(Qt.black, 1))
        painter.drawLine(0, 150, 300, 150)
        painter.drawLine(150, 0, 150, 300)
        painter.setPen(QPen(Qt.red, 20))
        painter.drawEllipse(QPoint(150, 150), 10, 10)
if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    main = Main()
    sys.exit(app.exec_())