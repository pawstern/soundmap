# Mapa dźwięku

## Przygotowanie

Run `sudo pip3 install -r requirements.txt` to install necessary modules.

## Uruchomienie

`python3 main.py`

## Opis

Po uruchomieniu otwiera się okno programu. Czerwona kropka po środku symbolizuje człowieka. Po kliknięciu w dowolne miejsce na oknie odtwarzany jest dźwięk bicia bębenków, przetworzony w taki sposób, żeby odzwierciedlał to jak słyszałby go człowiek umieszczony po środku mapy, gdyby źródłem dźwięku był kliknięty punkt.

## Źródła

[Baza HRIR](http://research.spa.aalto.fi/publications/papers/aes133-hrtf/)

[Problem z polieczeniem splotu sygnałów dźwiękowych](https://stackoverflow.com/posts/26028656/revisions)

[Źródło pliku utility.py](https://github.com/mgeier/python-audio/tree/master/audio-files)

[Źródło pliku drum.wav](http://www.frisnit.com/generating-binaural-sounds-for-immersive-audio-environments/)