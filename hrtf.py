from scipy.io import wavfile, loadmat
from scipy.signal import fftconvolve
import numpy as np
from utility import pcm2float, float2pcm
import os


def createTracks(input_filename, output_directory, hrtf_model):

    hrtf = loadmat(hrtf_model)

    input_rate,input_sig=wavfile.read(input_filename)
    input_sig=pcm2float(input_sig,'float32')
    for i in range(72):
        out_0=fftconvolve(input_sig,hrtf['IR_far_L'][:,i])
        out_0=out_0/np.max(np.abs(out_0))

        out_1=fftconvolve(input_sig,hrtf['IR_far_R'][:,i])
        out_1=out_1/np.max(np.abs(out_1))

        out=np.vstack((out_0,out_1)).T
        
        if not os.path.exists(output_directory):
            os.makedirs(output_directory)
        
        wavfile.write(output_directory+'/track{}_far.wav'.format(i),input_rate,float2pcm(out,'int32'))

    for i in range(72):
        out_0=fftconvolve(input_sig,hrtf['IR_near_R'][:,i])
        out_0=out_0/np.max(np.abs(out_0))

        out_1=fftconvolve(input_sig,hrtf['IR_near_L'][:,i])
        out_1=out_1/np.max(np.abs(out_1))

        out=np.vstack((out_0,out_1)).T
        
        wavfile.write(output_directory+'/track{}_near.wav'.format(i),input_rate,float2pcm(out,'int32'))

